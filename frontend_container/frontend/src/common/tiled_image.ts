export class TiledImage {
    size_x: number;
    size_y: number;
    pixel_count: number;
    tile_size_x: number;
    tile_size_y: number;
    tile_count_x: number;
    tile_count_y: number;
    scale_z: number;
    image_url: string;
    level_name: string;
    canvas: HTMLCanvasElement;
    ctx: any;
    onload_callback: any;
    requested: boolean[];
    failed: boolean[];
    image_tiles: HTMLImageElement[];
    data_tiles: Uint8Array[];
    constructor(size_x: number, size_y: number, tile_size_x: number, tile_size_y: number, scale_z: number, canvas: HTMLCanvasElement, image_url: string, level_name: string, onload_callback: () => void) {
        this.size_x = size_x;
        this.size_y = size_y;
        this.pixel_count = this.size_y * this.size_y

        this.tile_size_x = tile_size_x;
        this.tile_size_y = tile_size_y;
        this.tile_count_x = Math.ceil(size_x / tile_size_x);
        this.tile_count_y = Math.ceil(size_y / tile_size_y);
        
        this.scale_z = scale_z;
        
        this.image_url = image_url;
        this.level_name = level_name;
        canvas.width = size_x;
        canvas.height = size_y;
        this.canvas = canvas;

        this.ctx = this.canvas.getContext('2d', {willReadFrequently: true});
        
        this.onload_callback = onload_callback || (() => { });
        let tile_count = this.tile_count_x * this.tile_count_y
        this.image_tiles = new Array(tile_count).fill(null);
        this.data_tiles = new Array(tile_count).fill(null);
        this.requested = new Array(tile_count).fill(false);
        this.failed = new Array(tile_count).fill(false);
        
    }

    image_to_tile_coordinates(x: number, y: number): number[] {
        return [Math.floor(x / this.tile_size_x), Math.floor(y / this.tile_size_y)];
    }
    tile_offset(x: number, y: number): number[] {
        return [x % this.tile_size_x, y % this.tile_size_y];
    }
    tile_index(x: number, y: number): number {
        const [x_index, y_index] = this.image_to_tile_coordinates(x, y);
        return x_index + y_index * this.tile_count_x;
    }

    is_data_loaded(x: number, y: number): boolean {
        return this.data_tiles[this.tile_index(x, y)] !== null;
    }
    is_data_requested(x: number, y: number): boolean {
        return this.requested[this.tile_index(x, y)];
    }
    load_data(x: number, y: number) {
        const [x_index, y_index] = this.image_to_tile_coordinates(x, y);
        const tile_index = this.tile_index(x, y);
        //console.log('Loading tile:', x_index, y_index);
        const body_obj = {
            "level_name": this.level_name,
            "x_index": x_index,
            "y_index": y_index
        }
        //console.log("req args", body_obj);
        if (this.requested[tile_index]) {
            return;
        }
        fetch(this.image_url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body_obj)
        })
            .then(response => response.blob())
            .then(blob => {
                const img = new Image();
                img.onload = () => {
                    //console.log("running img onload")
                    this.image_tiles[tile_index] = img;
                    this.ctx.drawImage(img, 0, 0);
                    this.data_tiles[tile_index] = this.ctx.getImageData(0, 0, this.tile_size_x, this.tile_size_y).data;
                    console.log('Tile loaded:', x_index, y_index);
                    this.onload_callback();
                };
                img.src = URL.createObjectURL(blob);
            })
            .catch(error => {
                console.error('Error fetching image:');
            });
        this.requested[x_index + y_index * this.tile_count_x] = true;
    }

    get(x: number, y: number) {
        if (!this.is_data_loaded(x, y)) {
            if (!this.is_data_requested(x, y)){
                console.log('Loading pixel:', x, y);
                this.load_data(x, y);
            }
            return null;
        }
        //console.log('Getting pixel:', x, y);
        //get pixel from cached array
        
        const data_tile = this.data_tiles[this.tile_index(x, y)];
        const [tile_x, tile_y] = this.tile_offset(x, y);
        const dataIndex = (tile_y * this.tile_size_x + tile_x) * 4
        const dataHigh = data_tile[dataIndex];
        const dataLow = data_tile[dataIndex + 1];
        const data = ((dataHigh << 8) + dataLow);
        const result = (data / 128.0) * this.scale_z;
        //console.log('Pixel data:', result);
        return result;
    }
}